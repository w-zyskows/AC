﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Algorithms_and_Computbility.Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void CanonicalWordGeneration()
        {
            ConfigData.Instance.automaton.alphabetSize = 2;

            Word word0 = Word.getWordFromOrder(0);
            Word word1 = Word.getWordFromOrder(1);
            Word word2 = Word.getWordFromOrder(2);
            Word word3 = Word.getWordFromOrder(3);
            Word word4 = Word.getWordFromOrder(4);
            Word word5 = Word.getWordFromOrder(5);
            Word word6 = Word.getWordFromOrder(6);
            Word word7 = Word.getWordFromOrder(7);
            Word word8 = Word.getWordFromOrder(8);

            Assert.IsTrue(word0.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0 }));
            Assert.IsTrue(word1.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 1 }));
            Assert.IsTrue(word2.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0, 0 }));
            Assert.IsTrue(word3.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0, 1 }));
            Assert.IsTrue(word4.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 1, 0 }));
            Assert.IsTrue(word5.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 1, 1 }));
            Assert.IsTrue(word6.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0, 0, 0 }));
            Assert.IsTrue(word7.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0, 0, 1 }));
            Assert.IsTrue(word8.symbols.SequenceEqual(new System.Collections.Generic.List<int>() { 0, 1, 0 }));
        }

        [TestMethod]
        public void CanonicalWordSetMergeAndPartition()
        {
            WordSet A = WordSet.CreateCanonical(500);
            Tuple<WordSet, WordSet> APartitioned = A.Partition(1, 10);
            WordSet AMerged = WordSet.Merge(APartitioned.Item1, APartitioned.Item2);
            Assert.IsTrue(AMerged.words.Count.Equals(A.words.Count));
        }

        [TestMethod]
        public void ToolTest()
        {
            ConfigData.Instance.automaton.alphabetSize = 2;
            Automaton test = Automaton.Deserialize("4,2,2,4,3,3,3,3,4,4");
            WordSet set = WordSet.CreateCanonical(2000);

            test.InitEndingStates(set);
            
            Word A = new Word() { symbols = new System.Collections.Generic.List<int>() { 0 } };         // Q2
            Word B = new Word() { symbols = new System.Collections.Generic.List<int>() { 0, 0, 1 } };   // Q3
            Word C = new Word() { symbols = new System.Collections.Generic.List<int>() { 0, 1, 1 } };   // Q3
            Word D = new Word() { symbols = new System.Collections.Generic.List<int>() { 0, 1, 0 } };   // Q3
            Word E = new Word() { symbols = new System.Collections.Generic.List<int>() { 1, 0, 1 } };   // Q4
            Word F = new Word() { symbols = new System.Collections.Generic.List<int>() { 1, 1, 1 } };   // Q4
            Word G = new Word() { symbols = new System.Collections.Generic.List<int>() { 1, 0, 0 } };   // Q4

            Assert.IsFalse(test.areInSameEquivalenceClass(A, B));
            Assert.IsFalse(test.areInSameEquivalenceClass(A, C));
            Assert.IsFalse(test.areInSameEquivalenceClass(A, D));
            Assert.IsFalse(test.areInSameEquivalenceClass(A, E));
            Assert.IsFalse(test.areInSameEquivalenceClass(A, F));
            Assert.IsFalse(test.areInSameEquivalenceClass(A, G));

            Assert.IsFalse(test.areInSameEquivalenceClass(B, E));
            Assert.IsFalse(test.areInSameEquivalenceClass(B, F));
            Assert.IsFalse(test.areInSameEquivalenceClass(B, G));

            Assert.IsFalse(test.areInSameEquivalenceClass(C, E));
            Assert.IsFalse(test.areInSameEquivalenceClass(C, F));
            Assert.IsFalse(test.areInSameEquivalenceClass(C, G));

            Assert.IsFalse(test.areInSameEquivalenceClass(D, E));
            Assert.IsFalse(test.areInSameEquivalenceClass(D, F));
            Assert.IsFalse(test.areInSameEquivalenceClass(D, G));
        }

        [TestMethod]
        public void ComparisionTest()
        {
            WordSet A = WordSet.CreateCanonical(1000);

            Automaton automaton = Automaton.Deserialize("4,2,2,4,3,3,3,3,4,4");
            Automaton automatonCopy = Automaton.Deserialize("4,2,2,4,3,3,3,3,4,4");
            Automaton automatonDiff = Automaton.Deserialize("5,3,2,3,4,3,4,5,4,5,5,5,5,5,5,5,5");

            automaton.InitEndingStates(A);
            automatonCopy.InitEndingStates(A);
            automatonDiff.InitEndingStates(A);

            Assert.IsTrue(Automaton.compareAutomatons(automaton, automatonCopy, A).Equals(1));
            Assert.IsFalse(Automaton.compareAutomatons(automaton, automatonDiff, A).Equals(1));
        }

        [TestMethod]
        public void AssessMinimalBoundOfEquivalnceClassesTest()
        {
            WordSet A = WordSet.CreateCanonical(2000);

            Automaton automaton1 = Automaton.Deserialize("4,2,3,4,2,3,2,4,2,3");
            Automaton automaton2 = Automaton.Deserialize("4,2,1,1,4,4,1,1,1,1");
            Automaton automaton3 = Automaton.Deserialize("5,3,1,2,3,2,3,4,3,4,5,4,5,5,5,5,5");
            Automaton automaton4 = Automaton.Deserialize("5,3,2,3,4,3,4,5,4,5,5,5,5,5,5,5,5");

            automaton1.InitEndingStates(A);
            automaton2.InitEndingStates(A);
            automaton3.InitEndingStates(A);
            automaton4.InitEndingStates(A);

            Assert.IsTrue(automaton1.AssessEquivalenceClassesNumber(A).Equals(4));
            Assert.IsTrue(automaton2.AssessEquivalenceClassesNumber(A).Equals(1));
            Assert.IsTrue(automaton3.AssessEquivalenceClassesNumber(A).Equals(5));
            Assert.IsTrue(automaton4.AssessEquivalenceClassesNumber(A).Equals(5));
        }
    }
}
