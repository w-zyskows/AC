﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace Algorithms_and_Computbility
{
    public static class MutexExtension
    {
        private static Mutex mutex;

        /// <summary>
        /// Extension that allows only one instance of a progrem to be run at given time.
        /// </summary>
        /// <param name="application"></param>
        /// <param name="uniqueName"></param>
        /// <returns></returns>
        public static bool IsOneTimeLaunch( this Application application, string uniqueName = null )
        {
            var applicationName = Path.GetFileName( Assembly.GetEntryAssembly().GetName().Name );
            uniqueName = uniqueName ?? string.Format( "{0}_{1}_{2}",
                Environment.MachineName,
                Environment.UserName,
                applicationName );

            application.Exit += ( sender, e ) => mutex.Dispose();
            bool isOneTimeLaunch;
            mutex = new Mutex( true, uniqueName, out isOneTimeLaunch );
            return isOneTimeLaunch;
        }
    }
}
