﻿using Algorithms_and_Computbility.Classes.PSO;
using Algorithms_and_Computbility.Classes.PSO.Constants;
using Algorithms_and_Computbility.XAML;
using Algorithms_and_Computbility.XAML.Custom_Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace Algorithms_and_Computbility
{
    class ConfigData : INotifyPropertyChanged
    {
        // singleton pattern
        static readonly ConfigData _instance = new ConfigData();
        public static ConfigData Instance { get { return _instance; } }
        private ConfigData() { }


        /**
         *  BINDING boiler-plate
         *  from http://stackoverflow.com/a/1316417
         **/
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private Automaton _automaton = Automaton.Deserialize("8,5,1,2,3,4,5,2,3,4,5,6,3,4,5,6,7,4,5,6,7,8,5,6,7,8,8,6,7,8,8,8,7,8,8,8,8,8,8,8,8,8");
        //"4,2,2,4,3,3,3,3,4,4"
        //"4,2,3,4,2,3,2,4,2,3"
        //"5,3,1,2,3,2,3,4,3,4,5,4,5,5,5,5,5"
        //"8,5,1,2,3,4,5,2,3,4,5,6,3,4,5,6,7,4,5,6,7,8,5,6,7,8,8,6,7,8,8,8,7,8,8,8,8,8,8,8,8,8"
        public Automaton automaton
        {
            get { return _automaton; }
            set { _automaton = value; }
        }

        private int _errorTolerance = 15;
        public int errorTolerance
        {
            get { return _errorTolerance; }
            set { SetField(ref _errorTolerance, value, "errorTolerance"); }
        }


		private bool _useWordLength = true;
		public bool useWordLength
		{
			get { return _useWordLength; }
			set 
			{ 
				SetField ( ref _useWordLength , value , "useWordLength" );
				OnPropertyChanged ( "minMaxRandomWords" );
			}
		}

		private int _maxCanonicalWordLength = 3;
		public int maxCanonicalWordLength
		{
			get { return _maxCanonicalWordLength; }
			set 
			{ 
				SetField ( ref _maxCanonicalWordLength , value , "maxCanonicalWordLength" );
				OnPropertyChanged ( "minMaxRandomWords" );
			}
		}

		private int _maxCanonicalOrder = 500;
		public int maxCanonicalOrder
        {
			get { return _maxCanonicalOrder; }
			set 
			{ 
				SetField ( ref _maxCanonicalOrder , value , "maxCanonicalOrder" );
				OnPropertyChanged ( "minMaxRandomWords" );
			}
        }

		private int _maxRandomOrder = 3000;
		public int maxRandomOrder
		{
			get { return _maxRandomOrder; }
			set { SetField ( ref _maxRandomOrder , value , "maxRandomOrder" ); }
		}

		private int _minMaxRandomWords = 2000;
		public int minMaxRandomWords
		{
			get { return _minMaxRandomWords; }
			set { SetField ( ref _minMaxRandomWords , value , "minMaxRandomWords" ); }
		}

        private int _maxIterations = 1;
        public int maxIterations
        {
            get { return _maxIterations; }
            set { SetField(ref _maxIterations, value, "maxIterations"); }
        }

        private int _currentTab = 0;
        public int currentTab
        {
            get { return _currentTab; }
            set { SetField(ref _currentTab, value, "currentTab"); }
        }

        private bool _isApproximation = false;
        public bool isApproximation
        {
            get { return _isApproximation; }
            set { SetField(ref _isApproximation, value, "isApproximation"); }
        }

        private int _approximationLimit = 12;
        public int approximationLimit
        {
            get { return _approximationLimit; }
            set { SetField(ref _approximationLimit, value, "approximationLimit"); }
        }

        private int _approximationStep = 3;
        public int approximationStep
        {
            get { return _approximationStep; }
            set { SetField(ref _approximationStep, value, "approximationStep"); }
        }

        private int _currentApproximation = 0;
        public int currentApproximation
        {
            get { return _currentApproximation; }
            set { SetField(ref _currentApproximation, value, "currentApproximation"); }
        }


        private ResultTab _resultTab;
        public ResultTab resultTab
        {
            get { return _resultTab; }
            set { _resultTab = value; }
        }

        private WordSet _wordSet = new WordSet( new List<Word>());
        public WordSet wordSet
        {
            get { return _wordSet; }
            set { _wordSet = value; }
        }

        /// <summary>
        /// Provides UI flow logic for performing calculations
        /// </summary>
        public void Calculate()
        {
            currentTab++;
            resultTab.loadInitialGraph();
            Thread thread = new Thread(_PerformPSO);
            thread.Start();
        }


        private void _PerformPSO()
        {
            // get c
            // find out how many words it takes to have all words of length at most c
            // store length
            // generate a wordset of length length (canonical) = ws1
            // generate a wordset of length 3*length (random, starting from length c) = ws2

            // split ws2 1:2, ws1 += first split, ws2 = remaining part2
            // ws1 = training, ws2 = testing
			int wordCount = 0;
			if ( _useWordLength )
			{
				for ( int currentLen = 1 ; currentLen <= maxCanonicalWordLength ; currentLen += 1 )
				{
					wordCount += (int)Math.Pow( automaton.alphabetSize , currentLen );
				}
			}
			else
			{
				wordCount = maxCanonicalOrder;
			}

            OptimizationConstants oConstants = new OptimizationConstants(100, Instance.maxIterations, 10,
				1 - ( double )Instance.errorTolerance / ( double )100 , wordCount , _maxRandomOrder );
            MovementConstants mConstants = new MovementConstants(0.7, 2, 2);

			WordSet canonicalWordSet = WordSet.CreateCanonical ( oConstants.MAX_CANONICAL_COUNT );
			WordSet randomWordSet = WordSet.CreateRandom ( oConstants.MAX_CANONICAL_COUNT , oConstants.MAX_RANDOM_COUNT , oConstants.MAX_CANONICAL_COUNT*3 );
			Tuple<WordSet , WordSet> partitionedRandom = randomWordSet.Partition ( 1 , 2 );

			WordSet trainingSet = WordSet.Merge ( canonicalWordSet , partitionedRandom.Item1 );
			WordSet testingSet = partitionedRandom.Item2;

			PSO pso = new PSO ( oConstants , mConstants , Instance.automaton , trainingSet , testingSet );
            Instance.currentApproximation = Instance.approximationStep;
            while (true) {
            var watch = Stopwatch.StartNew();
            Tuple<double, Automaton/*, WordSet*/> output = pso.PerformOptimization();
            watch.Stop();
            double elapsedMs = (double)watch.ElapsedMilliseconds / 1000;

            if (Instance.isApproximation) {
                resultTab.Dispatcher.Invoke(new SetResultCallback(incrementTime), new object[] { elapsedMs });
                resultTab.Dispatcher.Invoke(new AutomatonTupleCallback(checkAccuracy), new object[] { output });
                currentApproximation += approximationStep;
                if(currentApproximation > approximationLimit) {
                    resultTab.Dispatcher.Invoke(new SetResultCallback(resultTab.setTime), new object[] { timeTaken });
                    resultTab.Dispatcher.Invoke(new SetResultCallback(resultTab.setResult), new object[] { currentAutomaton.Item1 });
                    resultTab.Dispatcher.Invoke(new SetResultGraphCallback(resultTab.setResultGraph), new object[] { currentAutomaton.Item2 });
                    currentTab++;
                    return;
                }
            }

            else {
                resultTab.Dispatcher.Invoke(new SetResultCallback(resultTab.setTime), new object[] { elapsedMs });
                resultTab.Dispatcher.Invoke(new SetResultCallback(resultTab.setResult), new object[] { output.Item1 });
                resultTab.Dispatcher.Invoke(new SetResultGraphCallback(resultTab.setResultGraph), new object[] { output.Item2 });
                currentTab++;
                    return;
            }
            }

            //System.Diagnostics.Debug.Write ( "\nresults" );
            //Automaton.compareAutomata ( Instance.automaton , output.Item2 , output.Item3 );
        }
        public delegate void SetTimeCallback(double timeTaken);
        public delegate void SetResultCallback(double newAccuracy);
        public delegate void SetResultGraphCallback(Automaton newAutomaton);
        public delegate void AutomatonTupleCallback(Tuple<double, Automaton> tuple);

        public Tuple<double, Automaton> currentAutomaton;
        public double timeTaken = 0;

        /// <summary>
        /// Delegate time measurement function
        /// </summary>
        /// <param name="time"></param>
        public void incrementTime(double time)
        {
            timeTaken += time;
        }
        /// <summary>
        /// Delegate comparision function
        /// </summary>
        /// <param name="automaton"></param>
        public void checkAccuracy(Tuple<double, Automaton> automaton)
        {
            if (currentAutomaton == null || automaton.Item1 > currentAutomaton.Item1)
                currentAutomaton = automaton;
        }
    }
}
