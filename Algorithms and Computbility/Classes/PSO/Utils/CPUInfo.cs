﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Utils
{
	internal static class CPUInfo
	{
		private const int THREADS_PER_CORE = 1;
		private static readonly int CORES;


		static CPUInfo ()
		{
			CORES = Environment.ProcessorCount;
		}

		public static int GetAvailableThreads ()
		{
			return THREADS_PER_CORE * CORES;
		}

		public static int GetAvailableThreads ( int threadCap )
		{
			return Math.Min ( THREADS_PER_CORE * CORES , threadCap );
		}
	}
}
