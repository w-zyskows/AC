﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Utils
{
	public class TwoKeyDictionary<K1 , K2 , T> :
	   Dictionary<K1 , Dictionary<K2 , T>>
	{
		public void Add ( K1 key1 , K2 key2 , T value )
		{
			if ( this.ContainsKey ( key1 ) )
			{
				if ( !this [ key1 ].ContainsKey ( key2 ) )
				{
					this [ key1 ] [ key2 ] = value;
				}
			}
			else
			{
				var temp = new Dictionary<K2 , T> ();
				temp [ key2 ] = value;
				this [ key1 ] = temp;
			}
		}
	}
}
