﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Internal
{
	internal class Space
	{
		private Automaton _perfectFit;
		private WordSet _fitnessSample;

		public readonly int StatesCount;
		public readonly int SymbolsCount;
		public readonly int Dimensions;

		public readonly double LowerBound;
		public readonly double UpperBound;
		private readonly double _shift;

		#region HELPER VARIABLES
		private Random _rnd;
		#endregion

		public Space ( int numberEquivalenceClasses , Automaton perfectFit , WordSet fitnessSample )
		{
			_perfectFit = perfectFit;
			_fitnessSample = fitnessSample;

			_perfectFit.InitEndingStates ( fitnessSample );

			StatesCount = numberEquivalenceClasses;
			SymbolsCount = _perfectFit.alphabetSize;
			Dimensions = StatesCount * SymbolsCount;

			double halfSpace = ( ( double )StatesCount - 1) / 2;
			LowerBound = -1 * ( halfSpace + 0.4999999 );
			UpperBound = 1 * ( halfSpace + 0.4999999 );

			_shift = 1 + halfSpace; // the amount of units we need to add to a position to raise the  minimum to 1

			_rnd = new Random ();
		}

		// hyperbolic normalization
		/*public void NormalizeSpeed ( double [] currentCoordinates , double [] currentSpeed )
		{
			for ( int i = 0 ; i < Dimensions ; i += 1 )
			{
				double coordinate = currentCoordinates [ i ];
				double speed = currentSpeed [ i ];

				if ( speed > 0 )
				{
					currentSpeed [ i ] = speed / ( 1 + Math.Abs ( speed / ( UpperBound - coordinate ) ) );
				}
				else
				{
					currentSpeed [ i ] = speed / ( 1 + Math.Abs ( speed / ( coordinate - LowerBound ) ) );
				}
			}
		}*/

		// reflect-z normalization
		public void NormalizeSpeed ( double [] currentCoordinates , double [] currentSpeed )
		{
			for ( int i = 0 ; i < Dimensions ; i += 1 )
			{
				double coordinate = currentCoordinates [ i ];
				double speed = currentSpeed [ i ];

				/*if ( speed > 0 )
				{
					currentSpeed [ i ] = speed / ( 1 + Math.Abs ( speed / ( UpperBound - coordinate ) ) );
				}
				else
				{
					currentSpeed [ i ] = speed / ( 1 + Math.Abs ( speed / ( coordinate - LowerBound ) ) );
				}*/
				double futurePosition = coordinate + speed;
				double normalizedFuturePosition = futurePosition;

				while ( normalizedFuturePosition > UpperBound || normalizedFuturePosition < LowerBound )
				{
					if ( normalizedFuturePosition > UpperBound )
					{
						normalizedFuturePosition = UpperBound - ( normalizedFuturePosition - UpperBound );
					}
					else //if ( normalizedFuturePosition < LowerBound )
					{
						normalizedFuturePosition = LowerBound + ( LowerBound - normalizedFuturePosition );
					}	
				}

				currentSpeed [ i ] = normalizedFuturePosition - coordinate;

			}
		}

		public double AssessQuality ( Position pos )
		{
			pos.Automaton.InitEndingStates ( _fitnessSample );
			return Automaton.compareAutomatons ( _perfectFit , pos.Automaton , _fitnessSample );
		}

		public double AssessQuality ( Automaton automaton , WordSet wordSet )
		{
			_perfectFit.InitEndingStates ( wordSet );
			automaton.InitEndingStates ( wordSet );
			return Automaton.compareAutomatons ( _perfectFit , automaton , wordSet );
		}

		public Automaton PositionToAutomaton ( double [] position )
		{
			int [] roundedPosition = new int [ position.Length ];
			for ( int i = 0 ; i < position.Length ; i += 1 )
			{
				roundedPosition [ i ] = ( int )Math.Round ( position [ i ] + _shift );
			}

			int [ , ] transitionMatrix = new int [ StatesCount , SymbolsCount ];
			for ( int symbol = 0 ; symbol < SymbolsCount ; symbol += 1 )
			{
				for ( int startingState = 0 ; startingState < StatesCount ; startingState += 1 )
				{
					int axis = symbol * StatesCount + startingState;
					double coordinateOnAxis = roundedPosition [ axis ];
					int actualEndingState = ( int )Math.Round ( coordinateOnAxis );
					transitionMatrix [ startingState , symbol ] = actualEndingState;
				}
			}
			return new Automaton ( StatesCount , SymbolsCount , transitionMatrix );
		}

		public double [] GetRandomSpeed ()
		{
			double [] speed = new double [ Dimensions ];
			for ( int i = 0 ; i < Dimensions ; i += 1 )
			{
				speed [ i ] = _rnd.NextDouble () * ( UpperBound - LowerBound ) + LowerBound;
			}
			return speed;
		}

		public double [] GetRandomCoordinates ()
		{
			double [] position = new double [ Dimensions ];
			for ( int i = 0 ; i < Dimensions ; i += 1 )
			{
				position [ i ] = _rnd.NextDouble () * ( UpperBound - LowerBound ) + LowerBound;
			}
			return position;
		}
	}
}
