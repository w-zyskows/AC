﻿using Algorithms_and_Computbility.Classes.PSO.Constants;
using Algorithms_and_Computbility.Classes.PSO.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Internal
{
	internal class Swarm
	{
		#region EXTERNAL REFERENCES
		private readonly Space _space;
		private readonly OptimizationConstants _oConstants;
		private readonly MovementConstants _mConstants;
		#endregion

		#region HELPER VARIABLES
		private Random _rnd;
		private object _lock;
		#endregion

		#region PARTICLES AND BEST KNOWN POSITION
		private List<Particle> _particles;

		private Position _best;
		public Position Best
		{
			get
			{
				return _best;
			}
		}
		#endregion

		#region INITIALIZATION
		public Swarm ( Space space , OptimizationConstants oConstants , MovementConstants mConstants )
		{
			_space = space;
			_oConstants = oConstants;
			_mConstants = mConstants;

			_rnd = new Random ();
			_lock = new object ();

			_InitRandomParticles ();
		}

		private void _InitRandomParticles ()
		{
			_particles = new List<Particle> ();

			int bestIndex = -1;
			double bestQuality = 0;

			for ( int i = 0 ; i < _oConstants.NUM_PARTICLES ; i += 1 )
			{
				Particle newParticle = new Particle ( _space , this );
				if ( bestIndex == -1 || bestQuality < newParticle.Best.Quality )
				{
					bestIndex = i;
					bestQuality = newParticle.Best.Quality;
				}

				_particles.Add ( newParticle );
			}

			_best = _particles [ bestIndex ].Best;
		}
		#endregion

		#region STEP
		public void PerformStepForAllParticles ()
		{
			int numThreads = CPUInfo.GetAvailableThreads ();
			int particlesPerThreadInt = _particles.Count / numThreads;
			int particlesPerThreadRemainder = _particles.Count % numThreads;

			ManualResetEvent [] doneEvents = new ManualResetEvent [ numThreads ];


			int firstParticle = 0;
			for ( int i = 0 ; i < numThreads ; i += 1 )
			{
				doneEvents [ i ] = new ManualResetEvent ( false );

				int actualParticlesPerThread = particlesPerThreadInt + (i < particlesPerThreadRemainder ? 1 : 0);
				List<Particle> particlesForCurrentThread = _particles.GetRange ( firstParticle , actualParticlesPerThread );
				firstParticle += actualParticlesPerThread;

				ThreadPool.QueueUserWorkItem ( ThreadCallback_MoveParticles ,
					new Tuple<List<Particle> , ManualResetEvent> ( particlesForCurrentThread , doneEvents [ i ] ) );

			}

			WaitHandle.WaitAll ( doneEvents );
		}
		#endregion

		#region THREAD POOL - CALLBACK
		private void ThreadCallback_MoveParticles ( Object threadContext )
		{
			Tuple<List<Particle> , ManualResetEvent> inputData = threadContext as Tuple<List<Particle> , ManualResetEvent>;
			List<Particle> particles = inputData.Item1;
			ManualResetEvent done = inputData.Item2;

			Position localBest = null;
			for ( int i = 0 ; i < particles.Count ; i += 1 )
			{
				Particle particle = particles [ i ];
				particle.Move ();
				if ( particle.Best.Quality > _best.Quality )
				{
					if ( localBest == null || particle.Best.Quality > localBest.Quality )
					{
						localBest = particle.Best;
					}
				}
			}

			if ( localBest != null )
			lock ( _lock )
			{
				if ( localBest.Quality > _best.Quality )
				{
					_best = localBest;
				}
			}

			done.Set ();
		}
		#endregion

		#region PUBLIC UTILS
		public void ChangeSpeed ( Particle particle )
		{
			Random rnd = new Random ();
			for ( int i = 0 ; i < _space.Dimensions ; i += 1 )
			{
				double weightA = rnd.NextDouble ();
				double weightB = rnd.NextDouble ();

				double distToParticleBest = particle.Best.Coordinates [ i ] - particle.Current.Coordinates [ i ];
				double distToSwarmBest = _best.Coordinates [ i ] - particle.Current.Coordinates [ i ];

				particle.Speed [ i ] = _mConstants.INERTIA * particle.Speed [ i ]
					+ _mConstants.PARTICLE_BEST_WEIGHT * weightA * distToParticleBest
					+ _mConstants.SWARM_BEST_WEIGHT * weightB * distToSwarmBest;
			}
		}
		#endregion
	}
}
