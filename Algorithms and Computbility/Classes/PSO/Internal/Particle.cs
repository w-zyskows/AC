﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Internal
{
	internal class Particle
	{
		#region EXTERNAL REFERENCES
		private readonly Space _space;
		private readonly Swarm _swarm;
		#endregion

		private Position _current;
		private Position _Current
		{
			set
			{
				_current = value;
				if ( _best == null || _current.Quality > _best.Quality )
				{
					_best = _current;
				}
			}
		}
		public Position Current
		{
			get
			{
				return _current;
			}
		}


		private double [] _speed;
		public double [] Speed
		{
			get
			{
				return _speed;
			}
		}

		private Position _best;
		public Position Best
		{
			get
			{
				return _best;
			}
		}

		public Particle ( Space space , Swarm swarm )
		{
			_space = space;
			_swarm = swarm;
			_Current = new Position ( _space );
		}

		public void Move ()
		{
			if ( _speed == null )
			{
				_speed = _space.GetRandomSpeed ();
			}
			else
			{
				_swarm.ChangeSpeed ( this );
			}
			_Current = _current.GetShiftedPosition ( _speed );
		}
	}
}
