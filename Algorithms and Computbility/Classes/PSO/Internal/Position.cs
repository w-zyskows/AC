﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Internal
{
	internal class Position
	{
		#region EXTERNAL REFERENCES
		private readonly Space _space;
		#endregion

		#region INTERNAL VARIABLES
		private double [] _coordinates;
		private double [] _Coordinates
		{
			set
			{
				_coordinates = value;
				_automaton = _space.PositionToAutomaton ( _coordinates );
				_quality = _space.AssessQuality ( this );
			}
		}
		public double [] Coordinates
		{
			get
			{
				return _coordinates;
			}
		}

		private double _quality;
		public double Quality
		{
			get
			{
				return _quality;
			}
		}
		private Automaton _automaton;
		public Automaton Automaton
		{ 
			get
			{
				return _automaton;
			}
		}
		#endregion

		#region CONSTRUCTORS
		public Position ( Space space )
		{
			_space = space;
			_Coordinates = _space.GetRandomCoordinates ();
		}

		public Position ( Space space, double [] coords )
		{
			_space = space;
			_Coordinates = coords;
		}
		#endregion

		public Position GetShiftedPosition ( double [] speed )
		{
			_space.NormalizeSpeed ( _coordinates , speed );
			double [] newCoordinates = new double[_space.Dimensions];
			for ( int i = 0 ; i < _space.Dimensions ; i += 1 )
			{
				newCoordinates [ i ] = _coordinates [ i ] + speed [ i ];
			}
			return new Position ( _space , newCoordinates );
		}

		public Position Clone ()
		{
			return new Position ( _space , (double[])_coordinates.Clone() );
		}
	}
}
