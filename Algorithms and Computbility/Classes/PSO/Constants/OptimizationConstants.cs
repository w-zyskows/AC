﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Constants
{
	public class OptimizationConstants
	{
		public readonly double NUM_PARTICLES;
		public readonly double NUM_ITERATIONS;
		public readonly double NUM_STEPS_PER_ITERATION;
		public readonly double DESIRED_ACCURACY;

		public readonly int MAX_CANONICAL_COUNT;
		public readonly int MAX_RANDOM_COUNT;

		public OptimizationConstants ( double numParticles , double numIterations , double numStepsPerIteration , double desiredAccuracy , 
			int maxCanonicalCount , int maxRandomCount )
		{
			NUM_PARTICLES = numParticles;
			NUM_ITERATIONS = numIterations;
			NUM_STEPS_PER_ITERATION = numStepsPerIteration;
			DESIRED_ACCURACY = desiredAccuracy;

			MAX_CANONICAL_COUNT = maxCanonicalCount;
			MAX_RANDOM_COUNT = maxRandomCount;
		}
	}
}
