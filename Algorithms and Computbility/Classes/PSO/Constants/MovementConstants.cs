﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO.Constants
{
	public class MovementConstants
	{
		public readonly double INERTIA;
		public readonly double PARTICLE_BEST_WEIGHT;
		public readonly double SWARM_BEST_WEIGHT;

		public MovementConstants ( double inertia , double particleWeight , double swarmWeight )
		{
			INERTIA = inertia;
			PARTICLE_BEST_WEIGHT = particleWeight;
			SWARM_BEST_WEIGHT = swarmWeight;
		}
	}
}
