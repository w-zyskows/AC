﻿using Algorithms_and_Computbility.Classes.PSO.Constants;
using Algorithms_and_Computbility.Classes.PSO.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility.Classes.PSO
{
	public class PSO
	{
		#region OPTIMIZATION INPUT
		private WordSet _trainingSet;
		private WordSet _testingSet;
		private Automaton _tool;
		#endregion

		#region OPTIMIZATION CONSTANT PARAMETERS
		private OptimizationConstants _oConstants;
		private MovementConstants _mConstants;
		#endregion

		#region INITIALIZATION
		public PSO ( OptimizationConstants oConstants , MovementConstants mConstants , Automaton automatonToReplicate , WordSet trainingSet , WordSet testingSet )
		{
			_oConstants = oConstants;
			_mConstants = mConstants;

			_tool = automatonToReplicate;
			_trainingSet = trainingSet;
			_testingSet = testingSet;
		}
		#endregion

		public Tuple<double , Automaton/*, WordSet*/> PerformOptimization ()
		{
			#region REFERENCES PLACED HERE FOR EASE OF DEBUGGING
			int iteration;

			int numEquivalenceClasses;
			Space space;
			Swarm swarm;

			Position best = null;

			int step;
			#endregion

			for ( iteration = 0 ; iteration < _oConstants.NUM_ITERATIONS ; iteration += 1 )
			{
				numEquivalenceClasses = ConfigData.Instance.isApproximation ? ConfigData.Instance.currentApproximation : _tool.AssessEquivalenceClassesNumber ( _trainingSet );
				space = new Space ( numEquivalenceClasses , _tool , _trainingSet );
				swarm = new Swarm ( space , _oConstants , _mConstants );

				for ( step = 0 ; step < _oConstants.NUM_STEPS_PER_ITERATION ; step += 1 )
				{
					swarm.PerformStepForAllParticles ();
					if ( swarm.Best.Quality > _oConstants.DESIRED_ACCURACY )
					{
						//return new Tuple<double , Automaton> ( swarm.Best.Quality , swarm.Best.Automaton );
						return new Tuple<double , Automaton /*, WordSet*/> ( space.AssessQuality ( swarm.Best.Automaton , _testingSet ) , swarm.Best.Automaton /*, testingWordSet */);
					}
				}

				// the swarm is recreated across iterations
				// imagine the following:
				// threshold - 95%, in the first iteration we reached 94%, in the next ones only 60%
				// hence we need to store the best position yielded by each iteration to return it in case we never reach the precision
				if ( best == null || best.Quality < swarm.Best.Quality )
				{
					best = swarm.Best;
					//wordsToTestBest = testingWordSet;
				}
				if ( iteration == _oConstants.NUM_ITERATIONS - 1 )
				{
					//return new Tuple<double , Automaton> ( best.Quality , best.Automaton);
					return new Tuple<double , Automaton /*, WordSet*/> ( space.AssessQuality ( best.Automaton , _testingSet ) , best.Automaton /*, wordsToTestBest*/ );
				}
			}

			throw new Exception ( "Error occurred during PSO" );
		}
	}
}
