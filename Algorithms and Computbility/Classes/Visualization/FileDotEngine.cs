﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Algorithms_and_Computbility.Classes.Visualization
{
	/// <summary>
	/// www.graphviz.org - GraphViz library - wrapper that converts a dot string describing a graph to an image
	/// </summary>
	public enum Renderer
	{
		//Dot=1,
		Default ,
		Neato ,
		Fdp ,
		Twopi ,
		Circo ,
	}

	class GraphVisualiser
	{
		public static string GraphVizBin = @"Classes\Visualization\dotlib\dot.exe";
		private string _outputType = "png";
		private Renderer _rendererType = Renderer.Default;

		public Renderer RendererType
		{
			get { return _rendererType; }
			set { _rendererType = value; }
		}

		public string GraphvizBin
		{
			get { return GraphVizBin; }
			set { GraphVizBin = value; }
		}

		public string OutputType
		{
			get { return _outputType; }
			set { _outputType = value; }
		}

		public bool OutputFile { get; set; }

		public Image OutputImage { get; private set; }
		//public Bitmap OutputImage { get; private set; }

		public string Run ( string dot )
		{
			string parameters = "";

			parameters = string.Format ( "{0} -T{1}" , parameters , _outputType );
			if ( RendererType != Renderer.Default )
			{
				parameters = string.Format ( "{0} -K{1}" , parameters , Enum.GetName ( typeof ( Renderer ) , RendererType ).ToLower () );
			}
			string directory = Path.GetDirectoryName ( Assembly.GetExecutingAssembly ().Location );
			string binCompletePath = Path.Combine ( directory , GraphvizBin );
			ProcessStartInfo pinfo = new ProcessStartInfo ( GraphvizBin , parameters );
			pinfo.UseShellExecute = false;
			pinfo.RedirectStandardInput = true;
			pinfo.RedirectStandardOutput = true;
			pinfo.RedirectStandardError = true;
			pinfo.WindowStyle = ProcessWindowStyle.Hidden;
			pinfo.CreateNoWindow = true;
			Process gvizProc = Process.Start ( pinfo );
			gvizProc.StandardInput.Write ( dot );
			gvizProc.StandardInput.Close ();

			if ( OutputFile == false )
			{
				System.Drawing.Image image = System.Drawing.Image.FromStream ( gvizProc.StandardOutput.BaseStream );
				OutputImage = /*(Bitmap)*/image;
			}
			string error = gvizProc.StandardError.ReadToEnd ();
			//if (!string.IsNullOrWhiteSpace(error))
			//	throw new Exception(error);
			gvizProc.WaitForExit ();
			return string.Empty;
		}
	}
}