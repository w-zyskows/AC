﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Algorithms_and_Computbility.Classes.Visualization
{
	class ImageUtils
	{
		public static System.Windows.Controls.Image ConvertDrawingImageToWPFImage ( System.Drawing.Image gdiImg )
		{
			System.Windows.Controls.Image img = new System.Windows.Controls.Image ();

			//convert System.Drawing.Image to WPF image
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap ( gdiImg );
			IntPtr hBitmap = bmp.GetHbitmap ();
			System.Windows.Media.ImageSource WpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap ( hBitmap , IntPtr.Zero , Int32Rect.Empty , BitmapSizeOptions.FromEmptyOptions () );

			img.Source = WpfBitmap;
			//img.Width = 500;
			//img.Height = 600;
			img.Stretch = System.Windows.Media.Stretch.None;
			return img;
		}
	}
}
