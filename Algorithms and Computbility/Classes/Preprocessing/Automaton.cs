﻿using Algorithms_and_Computbility.Classes.PSO.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility
{
    public class Automaton
    {

        private Dictionary<Word, int> _endingStates;

        private int _stateSize;
        public int stateSize
        {
            get { return _stateSize; }
            set { _stateSize = value; }
        }

        private int _alphabetSize;
        public int alphabetSize
        {
            get { return _alphabetSize; }
            set { _alphabetSize = value; }
        }

        private int[,] _transitionMatrix;
        public int[,] transitionMatrix
        {
            get { return _transitionMatrix; }
            set { _transitionMatrix = value; }
        }

        private List<List<Word>> _wordGroups = new List<List<Word>>();
        public List<List<Word>> wordGroups
        {
            get { return _wordGroups; }
            set { _wordGroups = value; }
        }

        public Automaton(int states, int symbols, int[,] matrix)
        {
            _stateSize = states;
            _alphabetSize = symbols;
            _transitionMatrix = matrix;
        }

        /// <summary>
        /// Checks wheter two words result in the same state.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
		public bool areInSameEquivalenceClass(Word a, Word b)
        {
            int stateA = _endingStates[a];//getWordEndingState(a);
            int stateB = _endingStates[b];//getWordEndingState(b);

            return stateA == stateB && stateA != -1 && stateB != -1;
        }

        private int getWordEndingState(Word word)
        {
            int currentState = 1;
            for (int i = 0; i < word.symbols.Count; i++) {
                currentState = getNextState(word.symbols.ElementAt(i), currentState);
            }
            return currentState;
        }

        private int getNextState(int symbol, int fromState)
        {
            if (transitionMatrix.GetUpperBound(0) < fromState - 1 || transitionMatrix.GetUpperBound(1) < symbol)
                return fromState;
            return transitionMatrix [ fromState - 1 , symbol ];
		}

        /// <summary>
        /// Having the wordset it assesses minimal bound of equivalence classes in the automaton, by grouping the words into equivalnece classes.
        /// </summary>
        /// <param name="wordSet"></param>
        /// <returns>Minimal bound for number of equivalence classes</returns>
		public int AssessEquivalenceClassesNumber ( WordSet wordSet )
		{
			InitEndingStates ( wordSet );
			_wordGroups = new List<List<Word>> ();

			for ( int i = 0 ; i < wordSet.words.Count ; i++ )
			{
				bool wasAdded = false;
				for ( int counter = 0 ; counter < wordGroups.Count ; counter++ )
				{
					if ( areInSameEquivalenceClass ( wordSet.words[ i ] , wordGroups[ counter ][ 0 ] ) )
					{
						wasAdded = true;
						wordGroups.ElementAt ( counter ).Add ( wordSet.words[ i ] );
					}
				}
				if ( !wasAdded )
				{
					List<Word> newGroup = new List<Word> ();
					newGroup.Add ( wordSet.words[ i ] );
					wordGroups.Add ( newGroup );
				}
			}

			return wordGroups.Count;
		}

        /// <summary>
        /// Populates Automatons equivalence groups.
        /// </summary>
        /// <param name="wordSet"></param>
		public void InitEndingStates ( WordSet wordSet )
		{
			_endingStates = new Dictionary<Word , int> ();
			foreach ( Word word in wordSet.words )
			{
				_endingStates.Add ( word , getWordEndingState ( word ) );
			}
		}

        /// <summary>
        /// Compares two automatons on a given wordset using tool allowing to check wheter two words are in a single equivalence class.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="wordSet"></param>
        /// <returns>Similarity result</returns>
		public static float compareAutomatons ( Automaton a , Automaton b , WordSet wordSet )
		{
			int passedTests = 0;
			int conductedTests = 0;
			//WordSet wordSet = ConfigData.Instance.wordSet;
			for ( int i = 0 ; i < wordSet.words.Count ; i++ )
			{
				for ( int x = i ; x < wordSet.words.Count ; x++ )
				{
					conductedTests++;
					if ( a.areInSameEquivalenceClass ( wordSet.words [ i ] , wordSet.words [ x ] ) == //a._answers [ wordSet.words [ i ] ] [ wordSet.words [ x ] ] ==
					   b.areInSameEquivalenceClass ( wordSet.words [ i ] , wordSet.words [ x ] ) )
					{
						passedTests++;
					}
				}
			}
			return ( ( float )passedTests ) / ( ( float )conductedTests );
		}
        /// <summary>
        /// Creates automaton from the input string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Constructed automaton</returns>
		public static Automaton Deserialize ( string input )
		{
			string [] positions = input.Split ( ',' );

			int states = Int32.Parse ( positions [ 0 ] );
			int symbols = Int32.Parse ( positions [ 1 ] );

			int count = states * symbols;

			int [ , ] transitionMatrix = new int [ states , symbols ];

			for ( int state = 0 ; state < states ; state += 1 )
			{
				for ( int symbol = 0 ; symbol < symbols ; symbol += 1 )
				{
					transitionMatrix [ state , symbol ] = Int32.Parse ( positions [ 2 + state * symbols + symbol ] );
				}
			}

			return new Automaton ( states , symbols , transitionMatrix );
		}

        /// <summary>
        /// Presents visual library with proper string representation
        /// </summary>
        /// <returns>Automaton string representation</returns>
		public string ToDotString ()
		{
			StringBuilder output = new StringBuilder ();
			output.Append ( "digraph G { label=\"" );
			//output.Append ( title );
			output.Append ( "\";\n" );

			for ( int i = 1 ; i <= stateSize ; i += 1 )
			{
				Dictionary<int , List<int>> transitions = new Dictionary<int , List<int>> ();
				for ( int j = 0 ; j < alphabetSize ; j += 1 )
				{
					int nextState = getNextState ( j , i );
					if ( !transitions.ContainsKey ( nextState ) )
					{
						transitions.Add ( nextState , new List<int> () );
					}
					transitions [ nextState ].Add ( j );
				}

				foreach ( KeyValuePair<int , List<int>> transitionList in transitions )
				{
					output.Append ( "q" );
					output.Append ( i );
					output.Append ( " -> " );
					output.Append ( "q" );
					output.Append ( transitionList.Key );
					output.Append ( "[ label=\" " );
					for ( int j = 0 ; j < transitionList.Value.Count ; j += 1 )
					{
						if ( j != 0 )
						{
							output.Append ( ", " );
						}
						output.Append ( transitionList.Value [ j ] );
					}
					output.Append ( " \" ];" );
				}
			}

			output.Append ( "}" );
			return output.ToString ();
		}
	}
}
