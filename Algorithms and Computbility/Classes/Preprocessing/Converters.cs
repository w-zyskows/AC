﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

/// <summary>
/// For XAML binding purpouses
/// </summary>
namespace Algorithms_and_Computbility.Converters
{
    public class InitialScreenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ConfigData.Instance.currentTab == 0)
                return Visibility.Visible;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

	public class MaxCanonicalOrderMinValue : IValueConverter
	{
		public object Convert ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			
			if ( ConfigData.Instance.useWordLength )
			{
				int wordCount = 0;
				for ( int currentLen = 1 ; currentLen <= ConfigData.Instance.maxCanonicalWordLength ; currentLen += 1 )
				{
					wordCount += ( int )Math.Pow ( ConfigData.Instance.automaton.alphabetSize , currentLen );
				}
				return wordCount * 4;
			}
			else
			{
				return ConfigData.Instance.maxCanonicalOrder * 4;
			}
		}

		public object ConvertBack ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			throw new NotImplementedException ();
		}
	}

	public class MaxWordLengthVisibility : IValueConverter
	{
		public object Convert ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			if ( ConfigData.Instance.useWordLength )
				return Visibility.Visible;
			else
				return Visibility.Hidden;
		}

		public object ConvertBack ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			throw new NotImplementedException ();
		}
	}

	public class WordSetLengthVisibility : IValueConverter
	{
		public object Convert ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			if ( !ConfigData.Instance.useWordLength )
				return Visibility.Visible;
			else
				return Visibility.Hidden;
		}

		public object ConvertBack ( object value , Type targetType , object parameter , CultureInfo culture )
		{
			throw new NotImplementedException ();
		}
	}


    public class ResultScreenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ConfigData.Instance.currentTab == 2)
                return Visibility.Visible;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LoadingScreenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ConfigData.Instance.currentTab == 1)
                return Visibility.Visible;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
