﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility
{
    public class WordSet
    {
        private List<Word> _words = new List<Word>();
        private int canonicalCount = 0;
        private int maxNumberOfFailedGenerations = 100;
        private float canonicalWordsPercentage = 0.5f;
        public List<Word> words
        {
            get { return _words; }
            set { _words = value; }
        }

		public WordSet ( List<Word> Words )
		{
			this.words = Words;
		}

        /// <summary>
        /// Creates wordset populated by words up to provided canonical order value.
        /// </summary>
        /// <param name="canonicalCount"></param>
        /// <returns></returns>
		public static WordSet CreateCanonical ( int canonicalCount )
		{
			List<Word> words = new List<Word> ();
			for ( int i = 0 ; i < canonicalCount ; i += 1 )
			{
				words.Add ( Word.getWordFromOrder ( i ) );
			}
			return new WordSet ( words );
		}

        /// <summary>
        /// Creates wordset populated by words of random canonical order.
        /// </summary>
        /// <param name="startingIndex"></param>
        /// <param name="finalIndex"></param>
        /// <param name="remainingWordsToPick"></param>
        /// <returns></returns>
		public static WordSet CreateRandom ( int startingIndex , int finalIndex , int remainingWordsToPick )
		{
			List<Word> words = new List<Word> ();

			double remainingWords = finalIndex - startingIndex + 1;

			Random rnd = new Random();

			for ( int i = startingIndex ; i < finalIndex + 1 ; i += 1 )
			{
				double likelihood = ( double )remainingWordsToPick / remainingWords;
				double nextRnd = rnd.NextDouble();
				if ( nextRnd < likelihood )
				{
					words.Add ( Word.getWordFromOrder ( i ) );
					remainingWordsToPick -= 1;
				}
				remainingWords -= 1;
			}

			return new WordSet ( words );
		}

        /// <summary>
        /// Connects two wordsets into one.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
		public static WordSet Merge ( WordSet first , WordSet second )
		{
			List<Word> words = new List<Word> ();
			words.AddRange ( first.words );
			words.AddRange ( second.words );
			return new WordSet ( words );
		}

        /// <summary>
        /// Divides a wordsets int two based on given ratio proportions.
        /// </summary>
        /// <param name="ratioFirst"></param>
        /// <param name="ratioSecond"></param>
        /// <returns>Returns tuple consisting of two ewly generated wordsets</returns>
		public Tuple<WordSet , WordSet> Partition (int ratioFirst , int ratioSecond)
		{
			int totalNumItems = this.words.Count;
			int numberFirst = (int)((double)ratioFirst / ((double) ( ratioFirst + ratioSecond )) * totalNumItems);

			List<Word> first = new List<Word> ();
			List<Word> second = new List<Word> ();
			Random rnd = new Random();

			for ( int i = 0 ; i < words.Count ; i += 1 )
			{
				double selectionProbability = (( double )numberFirst) / ((double)(totalNumItems - i));
				if ( rnd.NextDouble() <= selectionProbability )
				{
					numberFirst -= 1;
					first.Add ( words [ i ] );
				}
				else
				{
					second.Add ( words [ i ] );
				}
			}

			WordSet firstWordSet = new WordSet ( first );
			WordSet secondWordSet = new WordSet ( second );

			return new Tuple<WordSet , WordSet> ( firstWordSet , secondWordSet );
		}
    }
}
