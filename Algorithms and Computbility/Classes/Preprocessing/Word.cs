﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms_and_Computbility
{
    public class Word
    {
        private List<int> _symbols = new List<int>();
        private static Random rnd = new Random();
        public List<int> symbols
        {
            get { return _symbols; }
            set { _symbols = value; }
        }

        /// <summary>
        /// Generates canonically wordOrderValueth word.
        /// </summary>
        /// <param name="wordOrderValue"></param>
        /// <returns></returns>
        public static Word getWordFromOrder(int wordOrderValue)
        {
            Word word = new Word();
            int alphabetSize = ConfigData.Instance.automaton.alphabetSize;

            int wordLength = 0;
            int count = 0;
            while ((count += IntPower(alphabetSize, ++wordLength)) <= wordOrderValue);
            count -= IntPower(alphabetSize, wordLength);
            wordOrderValue -= count;

            for(int i = wordLength - 1; i >= 0; i--)
            {
                word.symbols.Add(wordOrderValue/ IntPower(alphabetSize, i));
                wordOrderValue = wordOrderValue % IntPower(alphabetSize, i);
            }

            return word;
        }

        private static int IntPower(int x, int power)
        {
            int val = 1;
            for(;power > 0; power--)
                val *= x;
            return val;
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null) {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Word p = obj as Word;
            if ((System.Object)p == null) {
                return false;
            }

            // Return true if the fields match:
            return (p.symbols.SequenceEqual(symbols));
        }
    }
}
