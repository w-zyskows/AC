﻿using System.Windows;

namespace Algorithms_and_Computbility
{
    public partial class MainWindow
    {

        public MainWindow()
        {
            if ( Application.Current.IsOneTimeLaunch() )
            {
                this.DataContext = ConfigData.Instance;
                InitializeComponent();
            }
            else
            {
                Application.Current.Shutdown();
            }  
        }
              
        private void MetroWindow_Loaded( object sender, RoutedEventArgs e )
        {

        }
    }
}
