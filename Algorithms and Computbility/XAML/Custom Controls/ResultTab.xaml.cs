﻿using System;
using System.Windows.Controls;

namespace Algorithms_and_Computbility.XAML
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ResultTab : UserControl
    {
        public Automaton resultAutomaton;
        public ResultTab()
        {
            InitializeComponent();
            ConfigData.Instance.resultTab = this;
        }

        public void loadInitialGraph()
        {
            graphInitial.Create(ConfigData.Instance.automaton);
        }

        public void setResultGraph(Automaton automaton)
        {
            graphResult.Create(automaton);
            this.resultAutomaton = automaton;
        }

		public void setTime ( double timeTaken )
		{
			TimeValue.Content = String.Format ( "{0:0.###}" , timeTaken );
		}

        public void setResult(double result)
        {
			double percentResult = result * 100;
			ResultValue.Content = String.Format ( "{0:0.###}" , percentResult );
        }
    }
}
