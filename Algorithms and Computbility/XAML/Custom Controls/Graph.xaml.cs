﻿using Algorithms_and_Computbility.Classes.Visualization;
using GraphX.Controls;
using GraphX.PCL.Common.Enums;
using GraphX.PCL.Common.Models;
using GraphX.PCL.Logic.Algorithms.LayoutAlgorithms;
using GraphX.PCL.Logic.Models;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Algorithms_and_Computbility.XAML.Custom_Controls
{

    //Layout visual class
    public class GraphAreaExample : GraphArea<DataVertex, DataEdge, BidirectionalGraph<DataVertex, DataEdge>> { }



    //Graph data class
    public class GraphExample : BidirectionalGraph<DataVertex, DataEdge> { }

    //Logic core class
    public class GXLogicCoreExample : GXLogicCore<DataVertex, DataEdge, BidirectionalGraph<DataVertex, DataEdge>> { }

    //Vertex data object
    public class DataVertex : VertexBase
    {
        /// <summary>
        /// Some string property for example purposes
        /// </summary>
        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    //Edge data object
    public class DataEdge : EdgeBase<DataVertex>
    {
        public DataEdge(DataVertex source, DataVertex target, double weight = 1)
            : base(source, target, weight)
        {
        }

        public DataEdge()
            : base(null, null, 1)
        {
        }

        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    /// <summary>
    /// Interaction logic for Graph.xaml
    /// </summary>
    public partial class Graph : UserControl
    {
        public Graph()
        {
            InitializeComponent();
            GraphX.Controls.ZoomControl.SetViewFinderVisibility(gg_zoomctrl, Visibility.Hidden);
        }

        /// <summary>
        /// Creates visual representation of a graph
        /// </summary>
        /// <param name="automaton"></param>
		public void Create ( Automaton automaton )
		{
			this.DisplayGrid.Children.Clear ();

			GraphVisualiser visualizer = new GraphVisualiser ();
			visualizer.OutputFile = false;
			visualizer.Run ( automaton.ToDotString() );
			Image graphImage = ImageUtils.ConvertDrawingImageToWPFImage ( visualizer.OutputImage );


			this.DisplayGrid.Children.Add ( graphImage );
		}
    }
}
