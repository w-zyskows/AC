﻿using Algorithms_and_Computbility.Classes.Visualization;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace Algorithms_and_Computbility.XAML
{
    /// <summary>
    /// Interaction logic for FunctionControl.xaml
    /// </summary>
    public partial class InitialTab : UserControl//, INotifyPropertyChanged
    {

		public System.Windows.Controls.Image GraphImage;

        public InitialTab()
        {
            this.DataContext = ConfigData.Instance;
            InitializeComponent();
            graphInit.Create(ConfigData.Instance.automaton);

			/*
			GraphVisualiser visualizer = new GraphVisualiser();
			visualizer.OutputFile = false;
			visualizer.Run(ConfigData.Instance.automaton.ToDotString());
			GraphImage = ImageUtils.ConvertDrawingImageToWPFImage ( visualizer.OutputImage );
			//graphInitial.Source = GraphImage;
			ImageGrid.Children.Clear ();
			ImageGrid.Children.Add ( GraphImage );
			//graphInitial.Source = Image;
			//PropertyCh ( "Image" );
			//Image = visualizer.OutputImage;
			//graphInitial.
			*/
        }

        private void Load_Automaton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter =
               "txt files (*.txt)|*.txt";
            dialog.InitialDirectory = "C:\\";
            dialog.Title = "Select a text file";
            if (dialog.ShowDialog() == true)
            {
                string content = System.IO.File.ReadAllText(dialog.FileName);
                try { 
                    ConfigData.Instance.automaton = Automaton.Deserialize(content);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("This is not a valid automaton text file! Please choose automaton representation text file accordingly to http://jastrzebska.mini.pw.edu.pl/Files/AnC/reprezentacja.pdf", "Error",
     MessageBoxButton.OK, MessageBoxImage.Error);
                }

				graphInit.Create ( ConfigData.Instance.automaton );
				/*

				GraphVisualiser visualizer = new GraphVisualiser ();
				visualizer.OutputFile = false;
				visualizer.Run ( ConfigData.Instance.automaton.ToDotString () );
				GraphImage = ImageUtils.ConvertDrawingImageToWPFImage ( visualizer.OutputImage );
				ImageGrid.Children.Clear ();
				ImageGrid.Children.Add ( GraphImage );
				 * 
				 * */
            }
        }

        private void CALCULATE_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ConfigData.Instance.Calculate();
        }
    }
}
